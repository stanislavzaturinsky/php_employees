--
-- Дамп данных таблицы `type_payments`
--

INSERT INTO `type_payments` (`payment_id`, `name`) VALUES
(1, 'Почасовая'),
(2, 'Месячная');

--
-- Дамп данных таблицы `departments`
--

INSERT INTO `departments` (`department_id`, `url_name`, `name`) VALUES
(1, 'logistics', 'Отдел логистики'),
(2, 'quality_control', 'Отдел контроля качества'),
(3, 'marketing', 'Отдел маркетинга');

--
-- Дамп данных таблицы `positions`
--

INSERT INTO `positions` (`position_id`, `department_id`, `url_name`, `name`) VALUES
(1, 1, 'director', 'Руководящая должность'),
(2, 1, 'manager', 'Менеджер по отправке/движению товара'),
(3, 2, 'director', 'Руководящая должность'),
(4, 2, 'specialist', 'Специалист по контролю качества'),
(5, 3, 'marketer', 'Маркетолог-аналитик'),
(6, 3, 'manager', 'Менеджер по продукту');



--
-- Дамп данных таблицы `employees`
--

INSERT INTO `employees` (`employee_id`, `full_name`, `birthday`, `department_id`, `position_id`, `payment_type_id`, `payment`) VALUES
(1, 'Сотрудник 1', '2/9/1999', 3, 5, 2, 398),
(2, 'Сотрудник 2', '15/9/1994', 2, 4, 1, 14),
(3, 'Сотрудник 3', '1/4/1997', 3, 6, 1, 16),
(4, 'Сотрудник 4', '17/10/1987', 1, 1, 2, 2715),
(5, 'Сотрудник 5', '27/6/1981', 2, 3, 1, 18),
(6, 'Сотрудник 6', '14/6/1998', 2, 3, 1, 12),
(7, 'Сотрудник 7', '16/9/1981', 3, 5, 2, 2590),
(8, 'Сотрудник 8', '4/8/1988', 2, 4, 2, 2446),
(9, 'Сотрудник 9', '15/10/1999', 1, 2, 1, 11),
(10, 'Сотрудник 10', '20/2/1983', 1, 1, 2, 2742),
(11, 'Сотрудник 11', '17/4/1989', 1, 2, 2, 1497),
(12, 'Сотрудник 12', '21/9/1991', 1, 1, 1, 12),
(13, 'Сотрудник 13', '22/8/1994', 1, 1, 1, 12),
(14, 'Сотрудник 14', '17/8/1988', 1, 2, 1, 8),
(15, 'Сотрудник 15', '22/8/1998', 3, 6, 1, 19),
(16, 'Сотрудник 16', '18/8/1986', 1, 2, 2, 331),
(17, 'Сотрудник 17', '2/3/2000', 2, 3, 1, 7),
(18, 'Сотрудник 18', '8/5/1996', 2, 4, 1, 8),
(19, 'Сотрудник 19', '8/7/1994', 1, 2, 2, 667),
(20, 'Сотрудник 20', '17/7/1994', 3, 6, 2, 2705),
(21, 'Сотрудник 21', '9/4/1999', 2, 3, 1, 7),
(22, 'Сотрудник 22', '7/4/1983', 1, 1, 2, 620),
(23, 'Сотрудник 23', '21/10/1998', 3, 6, 1, 13),
(24, 'Сотрудник 24', '27/3/1984', 3, 5, 2, 727),
(25, 'Сотрудник 25', '27/3/1985', 2, 4, 2, 1932),
(26, 'Сотрудник 26', '16/9/1996', 3, 6, 2, 2321),
(27, 'Сотрудник 27', '10/3/1996', 1, 1, 2, 406),
(28, 'Сотрудник 28', '13/7/2000', 3, 6, 2, 1234),
(29, 'Сотрудник 29', '24/9/1982', 2, 3, 1, 7),
(30, 'Сотрудник 30', '21/8/1988', 2, 4, 2, 1317),
(31, 'Сотрудник 31', '22/6/1986', 1, 2, 1, 5),
(32, 'Сотрудник 32', '12/6/1989', 2, 4, 2, 1609),
(33, 'Сотрудник 33', '11/2/1982', 2, 3, 1, 19),
(34, 'Сотрудник 34', '22/5/1987', 2, 4, 2, 2417),
(35, 'Сотрудник 35', '2/7/1999', 3, 5, 2, 1391),
(36, 'Сотрудник 36', '16/12/1994', 2, 3, 2, 586),
(37, 'Сотрудник 37', '15/3/1988', 3, 5, 2, 1984),
(38, 'Сотрудник 38', '7/12/1988', 1, 2, 2, 980),
(39, 'Сотрудник 39', '1/1/1990', 1, 2, 1, 14),
(40, 'Сотрудник 40', '6/10/1984', 3, 6, 1, 8),
(41, 'Сотрудник 41', '19/2/1994', 2, 4, 1, 11),
(42, 'Сотрудник 42', '5/9/1997', 2, 4, 1, 13),
(43, 'Сотрудник 43', '21/11/1988', 1, 2, 2, 1595),
(44, 'Сотрудник 44', '5/1/1982', 2, 3, 2, 1436),
(45, 'Сотрудник 45', '21/11/1982', 2, 3, 2, 2689),
(46, 'Сотрудник 46', '2/6/1988', 3, 5, 1, 20),
(47, 'Сотрудник 47', '24/8/1993', 2, 4, 2, 1615),
(48, 'Сотрудник 48', '19/5/1997', 3, 5, 2, 2127),
(49, 'Сотрудник 49', '1/7/1986', 2, 4, 2, 998),
(50, 'Сотрудник 50', '28/10/1988', 2, 3, 2, 1194),
(51, 'Сотрудник 51', '14/4/1990', 1, 1, 1, 13),
(52, 'Сотрудник 52', '14/12/1989', 3, 5, 1, 13),
(53, 'Сотрудник 53', '17/12/1990', 1, 2, 2, 757),
(54, 'Сотрудник 54', '13/6/1993', 3, 5, 2, 2606),
(55, 'Сотрудник 55', '11/6/2000', 1, 2, 1, 20),
(56, 'Сотрудник 56', '28/6/1989', 3, 5, 2, 413),
(57, 'Сотрудник 57', '22/7/1984', 1, 2, 2, 2388),
(58, 'Сотрудник 58', '14/12/1987', 2, 3, 1, 10),
(59, 'Сотрудник 59', '19/3/1988', 1, 2, 2, 2361),
(60, 'Сотрудник 60', '24/1/1999', 3, 6, 2, 2229),
(61, 'Сотрудник 61', '13/11/1995', 2, 4, 2, 2263),
(62, 'Сотрудник 62', '18/3/1987', 2, 4, 2, 518),
(63, 'Сотрудник 63', '29/12/1996', 3, 6, 1, 9),
(64, 'Сотрудник 64', '11/5/1988', 3, 5, 2, 1886),
(65, 'Сотрудник 65', '2/5/1993', 3, 5, 1, 7),
(66, 'Сотрудник 66', '26/6/1999', 2, 4, 1, 18),
(67, 'Сотрудник 67', '14/9/1988', 3, 6, 1, 12),
(68, 'Сотрудник 68', '12/7/1998', 2, 3, 1, 8),
(69, 'Сотрудник 69', '15/10/1983', 1, 2, 2, 2483),
(70, 'Сотрудник 70', '25/11/1997', 1, 1, 1, 19),
(71, 'Сотрудник 71', '25/2/1985', 1, 2, 2, 2194),
(72, 'Сотрудник 72', '5/4/1999', 2, 4, 2, 1818),
(73, 'Сотрудник 73', '6/5/1997', 3, 5, 1, 16),
(74, 'Сотрудник 74', '15/8/1987', 2, 4, 2, 553),
(75, 'Сотрудник 75', '7/3/1995', 3, 5, 1, 19),
(76, 'Сотрудник 76', '14/9/1990', 2, 4, 2, 911),
(77, 'Сотрудник 77', '1/12/1999', 1, 1, 1, 15),
(78, 'Сотрудник 78', '11/9/1985', 3, 6, 2, 2017),
(79, 'Сотрудник 79', '7/6/1995', 1, 2, 1, 12),
(80, 'Сотрудник 80', '17/6/1984', 3, 6, 2, 885),
(81, 'Сотрудник 81', '12/9/1981', 2, 4, 1, 16),
(82, 'Сотрудник 82', '16/7/1998', 3, 6, 1, 10),
(83, 'Сотрудник 83', '3/11/1994', 3, 6, 1, 9),
(84, 'Сотрудник 84', '8/9/1981', 2, 3, 2, 474),
(85, 'Сотрудник 85', '7/7/1988', 3, 6, 2, 1762),
(86, 'Сотрудник 86', '14/1/2000', 3, 6, 1, 12),
(87, 'Сотрудник 87', '12/1/1989', 1, 2, 2, 2391),
(88, 'Сотрудник 88', '25/2/1992', 1, 1, 1, 17),
(89, 'Сотрудник 89', '28/9/1992', 3, 6, 1, 10),
(90, 'Сотрудник 90', '8/8/1996', 2, 3, 2, 2495),
(91, 'Сотрудник 91', '11/12/1989', 3, 5, 1, 18),
(92, 'Сотрудник 92', '2/11/1996', 1, 2, 1, 20),
(93, 'Сотрудник 93', '13/12/1995', 2, 4, 1, 16),
(94, 'Сотрудник 94', '11/2/1983', 2, 4, 2, 2082),
(95, 'Сотрудник 95', '18/3/1992', 2, 3, 1, 20),
(96, 'Сотрудник 96', '7/8/1985', 2, 4, 1, 14),
(97, 'Сотрудник 97', '3/7/1992', 1, 1, 2, 2526),
(98, 'Сотрудник 98', '26/12/1987', 1, 2, 2, 2422),
(99, 'Сотрудник 99', '9/10/1985', 2, 3, 2, 2768),
(100, 'Сотрудник 100', '4/7/2000', 3, 5, 1, 7),
(101, 'Сотрудник 101', '25/4/1985', 1, 1, 1, 10),
(102, 'Сотрудник 102', '10/2/1982', 1, 1, 2, 1576),
(103, 'Сотрудник 103', '2/3/1989', 1, 1, 2, 815),
(104, 'Сотрудник 104', '29/2/1989', 2, 4, 1, 17),
(105, 'Сотрудник 105', '3/6/1988', 3, 5, 2, 1596),
(106, 'Сотрудник 106', '21/11/1995', 2, 3, 1, 13),
(107, 'Сотрудник 107', '3/6/1989', 2, 3, 1, 15),
(108, 'Сотрудник 108', '19/9/1996', 2, 3, 1, 19),
(109, 'Сотрудник 109', '21/11/1983', 2, 4, 1, 18),
(110, 'Сотрудник 110', '18/4/1980', 1, 1, 2, 711),
(111, 'Сотрудник 111', '2/3/1985', 3, 5, 2, 2401),
(112, 'Сотрудник 112', '11/12/1990', 1, 2, 2, 2006),
(113, 'Сотрудник 113', '4/10/1984', 1, 1, 1, 13),
(114, 'Сотрудник 114', '29/9/1985', 2, 3, 2, 862),
(115, 'Сотрудник 115', '2/5/1984', 2, 4, 2, 1382),
(116, 'Сотрудник 116', '27/11/1992', 2, 3, 2, 1553),
(117, 'Сотрудник 117', '9/1/1994', 2, 3, 1, 9),
(118, 'Сотрудник 118', '12/12/1984', 1, 1, 1, 20),
(119, 'Сотрудник 119', '24/7/1985', 1, 1, 2, 2868),
(120, 'Сотрудник 120', '18/3/1992', 3, 6, 2, 1474),
(121, 'Сотрудник 121', '16/9/1986', 1, 2, 2, 938),
(122, 'Сотрудник 122', '27/7/1982', 3, 5, 1, 7),
(123, 'Сотрудник 123', '13/9/1997', 2, 3, 2, 1206),
(124, 'Сотрудник 124', '6/2/1991', 1, 2, 2, 504),
(125, 'Сотрудник 125', '24/11/1982', 1, 2, 1, 5),
(126, 'Сотрудник 126', '1/5/1998', 1, 1, 1, 6),
(127, 'Сотрудник 127', '18/5/1996', 3, 6, 2, 2893),
(128, 'Сотрудник 128', '28/4/2000', 3, 6, 1, 11),
(129, 'Сотрудник 129', '16/7/1998', 2, 3, 2, 2560),
(130, 'Сотрудник 130', '23/10/1993', 1, 2, 1, 14),
(131, 'Сотрудник 131', '13/9/1990', 3, 6, 1, 16),
(132, 'Сотрудник 132', '21/10/1999', 2, 3, 2, 2978),
(133, 'Сотрудник 133', '17/9/1995', 1, 1, 1, 9),
(134, 'Сотрудник 134', '21/9/1985', 3, 5, 2, 2718),
(135, 'Сотрудник 135', '19/2/2000', 1, 1, 2, 448),
(136, 'Сотрудник 136', '21/5/1998', 1, 1, 1, 18),
(137, 'Сотрудник 137', '10/6/1986', 3, 6, 1, 14),
(138, 'Сотрудник 138', '24/8/1980', 1, 2, 2, 2415),
(139, 'Сотрудник 139', '11/8/1991', 1, 2, 1, 18),
(140, 'Сотрудник 140', '21/2/2000', 3, 5, 1, 12),
(141, 'Сотрудник 141', '23/9/1995', 2, 4, 2, 1231),
(142, 'Сотрудник 142', '20/4/1993', 3, 5, 1, 13),
(143, 'Сотрудник 143', '7/10/1982', 1, 1, 2, 1612),
(144, 'Сотрудник 144', '1/10/1993', 1, 1, 2, 2789),
(145, 'Сотрудник 145', '8/10/1994', 3, 6, 1, 16),
(146, 'Сотрудник 146', '9/3/1990', 3, 5, 1, 20),
(147, 'Сотрудник 147', '13/9/1983', 3, 6, 2, 2745),
(148, 'Сотрудник 148', '14/5/2000', 3, 6, 1, 16),
(149, 'Сотрудник 149', '20/4/1987', 3, 6, 1, 11),
(150, 'Сотрудник 150', '7/4/1986', 1, 2, 1, 9),
(151, 'Сотрудник 151', '17/11/1990', 1, 1, 2, 1658),
(152, 'Сотрудник 152', '8/8/1989', 1, 2, 2, 2848),
(153, 'Сотрудник 153', '15/9/1987', 3, 6, 1, 13),
(154, 'Сотрудник 154', '14/12/1987', 2, 3, 1, 11),
(155, 'Сотрудник 155', '26/4/1992', 1, 2, 1, 7),
(156, 'Сотрудник 156', '6/10/1982', 1, 1, 2, 485),
(157, 'Сотрудник 157', '17/8/1998', 3, 6, 1, 16),
(158, 'Сотрудник 158', '16/1/1988', 1, 1, 1, 12),
(159, 'Сотрудник 159', '28/6/1987', 2, 3, 2, 1260),
(160, 'Сотрудник 160', '17/4/1991', 1, 1, 1, 7),
(161, 'Сотрудник 161', '10/6/1992', 2, 3, 2, 907),
(162, 'Сотрудник 162', '16/12/1997', 2, 3, 2, 1077),
(163, 'Сотрудник 163', '29/6/1982', 2, 4, 1, 19),
(164, 'Сотрудник 164', '5/7/1995', 2, 3, 1, 16),
(165, 'Сотрудник 165', '20/12/1992', 1, 2, 1, 14),
(166, 'Сотрудник 166', '28/9/1986', 3, 5, 2, 822),
(167, 'Сотрудник 167', '18/11/1998', 1, 1, 2, 1094),
(168, 'Сотрудник 168', '8/1/1984', 3, 6, 1, 17),
(169, 'Сотрудник 169', '29/5/1991', 3, 5, 2, 483),
(170, 'Сотрудник 170', '24/8/1983', 1, 1, 1, 20),
(171, 'Сотрудник 171', '29/8/1980', 2, 3, 1, 10),
(172, 'Сотрудник 172', '19/12/1980', 3, 6, 2, 2713),
(173, 'Сотрудник 173', '7/6/1994', 3, 6, 2, 970),
(174, 'Сотрудник 174', '19/8/1983', 3, 5, 2, 2489),
(175, 'Сотрудник 175', '23/4/1988', 2, 3, 2, 1836),
(176, 'Сотрудник 176', '17/12/1994', 1, 1, 2, 534),
(177, 'Сотрудник 177', '22/8/1991', 2, 4, 2, 1135),
(178, 'Сотрудник 178', '15/2/1992', 1, 1, 1, 14),
(179, 'Сотрудник 179', '5/12/1989', 2, 4, 1, 17),
(180, 'Сотрудник 180', '25/4/1984', 2, 4, 1, 19),
(181, 'Сотрудник 181', '14/7/1988', 1, 1, 1, 19),
(182, 'Сотрудник 182', '26/12/1985', 1, 1, 2, 1772),
(183, 'Сотрудник 183', '22/1/1982', 3, 6, 1, 11),
(184, 'Сотрудник 184', '23/6/1983', 2, 3, 1, 10),
(185, 'Сотрудник 185', '24/6/1997', 3, 5, 2, 837),
(186, 'Сотрудник 186', '26/9/1988', 2, 3, 1, 12),
(187, 'Сотрудник 187', '21/9/1995', 2, 4, 2, 1401),
(188, 'Сотрудник 188', '2/9/1992', 3, 5, 1, 19),
(189, 'Сотрудник 189', '21/4/1980', 1, 2, 1, 18),
(190, 'Сотрудник 190', '28/1/1998', 2, 4, 2, 2047),
(191, 'Сотрудник 191', '21/5/1985', 2, 3, 1, 7),
(192, 'Сотрудник 192', '2/9/1980', 1, 1, 1, 16),
(193, 'Сотрудник 193', '15/3/1993', 2, 3, 1, 20),
(194, 'Сотрудник 194', '5/7/1998', 1, 2, 1, 18),
(195, 'Сотрудник 195', '27/2/1982', 3, 5, 1, 12),
(196, 'Сотрудник 196', '15/8/1988', 3, 6, 1, 9),
(197, 'Сотрудник 197', '4/1/1997', 3, 6, 1, 9),
(198, 'Сотрудник 198', '2/2/1984', 1, 2, 1, 10),
(199, 'Сотрудник 199', '28/7/1983', 3, 6, 2, 2754),
(200, 'Сотрудник 200', '21/1/1988', 1, 1, 1, 14),
(201, 'Сотрудник 201', '1/1/1984', 2, 4, 2, 2796),
(202, 'Сотрудник 202', '19/1/1983', 1, 1, 1, 20),
(203, 'Сотрудник 203', '1/7/1997', 3, 5, 1, 9),
(204, 'Сотрудник 204', '14/2/1999', 1, 2, 1, 8),
(205, 'Сотрудник 205', '1/1/2000', 2, 4, 1, 13),
(206, 'Сотрудник 206', '14/5/1991', 2, 3, 2, 2872),
(207, 'Сотрудник 207', '22/9/1980', 2, 3, 2, 372),
(208, 'Сотрудник 208', '1/7/1991', 3, 5, 2, 642),
(209, 'Сотрудник 209', '18/4/1997', 3, 5, 2, 1000),
(210, 'Сотрудник 210', '17/5/1992', 1, 1, 2, 532),
(211, 'Сотрудник 211', '8/1/1982', 2, 4, 2, 408),
(212, 'Сотрудник 212', '5/7/1987', 2, 3, 1, 19),
(213, 'Сотрудник 213', '19/7/1997', 1, 2, 2, 2405),
(214, 'Сотрудник 214', '13/1/1982', 3, 5, 2, 2070),
(215, 'Сотрудник 215', '8/2/1981', 2, 3, 2, 2275),
(216, 'Сотрудник 216', '5/8/1996', 2, 4, 1, 14),
(217, 'Сотрудник 217', '16/2/1994', 1, 2, 2, 956),
(218, 'Сотрудник 218', '5/10/1987', 1, 2, 1, 12),
(219, 'Сотрудник 219', '27/1/1985', 2, 4, 2, 317),
(220, 'Сотрудник 220', '24/2/2000', 3, 6, 1, 15),
(221, 'Сотрудник 221', '6/10/1991', 3, 6, 1, 9),
(222, 'Сотрудник 222', '7/6/1983', 1, 2, 1, 16),
(223, 'Сотрудник 223', '25/3/1992', 2, 3, 2, 1033),
(224, 'Сотрудник 224', '13/7/1987', 1, 2, 1, 8),
(225, 'Сотрудник 225', '2/11/1981', 2, 3, 2, 638),
(226, 'Сотрудник 226', '20/7/1996', 1, 1, 1, 9),
(227, 'Сотрудник 227', '6/9/1990', 3, 6, 1, 11),
(228, 'Сотрудник 228', '22/1/2000', 2, 3, 2, 2332),
(229, 'Сотрудник 229', '29/1/1988', 2, 4, 1, 10),
(230, 'Сотрудник 230', '2/11/2000', 1, 2, 2, 1418),
(231, 'Сотрудник 231', '20/1/1997', 1, 2, 1, 19),
(232, 'Сотрудник 232', '2/5/1989', 2, 4, 1, 9),
(233, 'Сотрудник 233', '12/9/1985', 1, 1, 2, 720),
(234, 'Сотрудник 234', '12/11/1981', 1, 2, 2, 2676),
(235, 'Сотрудник 235', '12/4/1980', 3, 5, 2, 1843),
(236, 'Сотрудник 236', '5/8/1995', 3, 5, 1, 11),
(237, 'Сотрудник 237', '7/7/1994', 2, 4, 1, 7),
(238, 'Сотрудник 238', '6/6/1996', 1, 1, 1, 20),
(239, 'Сотрудник 239', '1/12/1980', 2, 3, 2, 2622),
(240, 'Сотрудник 240', '9/7/1987', 3, 6, 2, 413),
(241, 'Сотрудник 241', '25/11/1989', 1, 1, 1, 14),
(242, 'Сотрудник 242', '15/1/1989', 2, 3, 1, 12),
(243, 'Сотрудник 243', '22/1/1983', 3, 5, 1, 15),
(244, 'Сотрудник 244', '9/3/1987', 1, 2, 1, 17),
(245, 'Сотрудник 245', '5/7/1993', 1, 1, 2, 372),
(246, 'Сотрудник 246', '26/7/1997', 3, 5, 2, 2031),
(247, 'Сотрудник 247', '5/2/1989', 1, 2, 2, 1402),
(248, 'Сотрудник 248', '13/3/1986', 3, 6, 2, 2244),
(249, 'Сотрудник 249', '18/9/1998', 1, 1, 2, 2088),
(250, 'Сотрудник 250', '10/4/1984', 3, 5, 1, 9),
(251, 'Сотрудник 251', '14/3/1983', 3, 5, 2, 2946),
(252, 'Сотрудник 252', '13/10/1983', 1, 2, 1, 9),
(253, 'Сотрудник 253', '3/3/1989', 3, 6, 2, 697),
(254, 'Сотрудник 254', '11/4/1987', 1, 1, 2, 2137),
(255, 'Сотрудник 255', '10/11/1986', 2, 3, 2, 2439),
(256, 'Сотрудник 256', '19/8/1991', 3, 6, 1, 14),
(257, 'Сотрудник 257', '29/5/1998', 1, 2, 1, 15),
(258, 'Сотрудник 258', '2/11/1992', 3, 5, 2, 1929),
(259, 'Сотрудник 259', '26/2/1984', 1, 1, 1, 17),
(260, 'Сотрудник 260', '25/11/1989', 3, 5, 1, 6),
(261, 'Сотрудник 261', '6/12/1988', 3, 5, 2, 2436),
(262, 'Сотрудник 262', '27/12/1993', 1, 1, 2, 1209),
(263, 'Сотрудник 263', '23/6/1998', 3, 5, 2, 1007),
(264, 'Сотрудник 264', '8/2/1987', 2, 4, 2, 2636),
(265, 'Сотрудник 265', '6/3/1993', 2, 4, 2, 2358),
(266, 'Сотрудник 266', '29/1/1997', 1, 2, 2, 2088),
(267, 'Сотрудник 267', '5/11/1993', 3, 5, 1, 8),
(268, 'Сотрудник 268', '21/7/1987', 3, 5, 2, 1475),
(269, 'Сотрудник 269', '23/11/1996', 1, 2, 2, 861),
(270, 'Сотрудник 270', '23/12/1999', 2, 4, 1, 18),
(271, 'Сотрудник 271', '21/1/1988', 2, 3, 2, 483),
(272, 'Сотрудник 272', '14/11/2000', 3, 6, 1, 18),
(273, 'Сотрудник 273', '14/4/1989', 2, 3, 2, 1069),
(274, 'Сотрудник 274', '10/1/1983', 1, 2, 1, 16),
(275, 'Сотрудник 275', '18/11/1992', 3, 6, 1, 15),
(276, 'Сотрудник 276', '10/3/1985', 3, 6, 2, 1882),
(277, 'Сотрудник 277', '3/4/1996', 1, 2, 1, 15),
(278, 'Сотрудник 278', '6/4/1988', 1, 1, 2, 1654),
(279, 'Сотрудник 279', '14/12/1983', 2, 3, 2, 1542),
(280, 'Сотрудник 280', '17/2/1994', 1, 2, 1, 16),
(281, 'Сотрудник 281', '3/2/1990', 1, 2, 2, 1531),
(282, 'Сотрудник 282', '11/2/1999', 1, 2, 2, 317),
(283, 'Сотрудник 283', '13/4/1989', 3, 5, 2, 1187),
(284, 'Сотрудник 284', '28/12/1986', 3, 5, 2, 2437),
(285, 'Сотрудник 285', '10/7/1994', 1, 2, 1, 10),
(286, 'Сотрудник 286', '12/6/1988', 2, 3, 1, 13),
(287, 'Сотрудник 287', '6/6/1987', 2, 3, 1, 16),
(288, 'Сотрудник 288', '23/3/1993', 3, 5, 1, 17),
(289, 'Сотрудник 289', '9/10/1981', 3, 6, 2, 1719),
(290, 'Сотрудник 290', '10/7/1985', 2, 4, 2, 2438),
(291, 'Сотрудник 291', '27/5/1987', 3, 6, 2, 1496),
(292, 'Сотрудник 292', '24/1/1996', 1, 2, 2, 1113),
(293, 'Сотрудник 293', '24/9/1996', 1, 1, 1, 14),
(294, 'Сотрудник 294', '20/6/1983', 3, 5, 2, 663),
(295, 'Сотрудник 295', '7/9/1985', 1, 2, 1, 5),
(296, 'Сотрудник 296', '10/4/1983', 3, 5, 1, 9),
(297, 'Сотрудник 297', '10/7/1989', 3, 5, 2, 2308),
(298, 'Сотрудник 298', '14/8/1989', 2, 3, 2, 1162),
(299, 'Сотрудник 299', '9/5/1992', 3, 5, 2, 1025),
(300, 'Сотрудник 300', '5/11/1999', 1, 2, 1, 10);