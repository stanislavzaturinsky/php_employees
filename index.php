<?php
include "models/Employees.php";
$employees = new Employees();
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta charset="utf-8">
    <title>Test exercise</title>
</head>
<body>
    <h2>Сотрудники</h2>
    <ul>
        <li>
            <span>XML</span>
            <ul>
                <li><span class="navigation" data-href="/xml/employees">Все сотрудники из XML</span></li>
            </ul>
        </li>
        <li>
            <span>DB</span>
            <ul>
                <li><span class="navigation" data-href="/db/employees">Все сотрудники из базы данных</span></li>
                <li>
                    <span class="navigation" data-href="/db/employees/logistics">Отдел логистики</span>
                    <ul>
                        <li><span class="navigation" data-href="/db/employees/logistics/director">Руководящая должность</span></li>
                        <li><span class="navigation" data-href="/db/employees/logistics/manager">Менеджер по отправке/движению товара</span></li>
                    </ul>
                </li>
                <li>
                    <span class="navigation" data-href="/db/employees/quality_control">Отдел контроля качества</span>
                    <ul>
                        <li><span class="navigation" data-href="/db/employees/quality_control/director">Руководящая должность</span></li>
                        <li><span class="navigation" data-href="/db/employees/quality_control/specialist">Специалист по контролю качества</span></li>
                    </ul>
                </li>
                <li>
                    <span class="navigation" data-href="/db/employees/marketing">Отдел маркетинга</span>
                    <ul>
                        <li><span class="navigation" data-href="/db/employees/marketing/marketer">Маркетолог-аналитик</span></li>
                        <li><span class="navigation" data-href="/db/employees/marketing/manager">Менеджер по продукту</span></li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>

    <h4>Количесто сотрудников на страницу</h4>
    <select id="quantity" onchange="changeQuantity()">
        <option value="20"  <?= ($employees->getQuantity() == 20)  ? 'selected="selected"' : '' ?>>20</option>
        <option value="40"  <?= ($employees->getQuantity() == 40)  ? 'selected="selected"' : '' ?>>40</option>
        <option value="50"  <?= ($employees->getQuantity() == 50)  ? 'selected="selected"' : '' ?>>50</option>
        <option value="100" <?= ($employees->getQuantity() == 100) ? 'selected="selected"' : '' ?>>100</option>
    </select><br>
    <table border="1">
        <tr id="header">
            <th>ФИО</th>
            <th>Дата рождения</th>
            <th>Отдел</th>
            <th>Должность</th>
            <th>Тип оплаты сотрудника</th>
            <th>Оплата за месяц</th>
        </tr>
<!--        Вывод сотрудников-->
        <?php if(!empty($employees->getRows())) {
            echo $employees->getRows();
        }?>
    </table>
    <div id="paginator" style="margin-top: 20px;">
<!--        Вывод пагинации-->
        <?php echo $employees->getPagination() ?>
    </div>
    <script>
        setPaginationEvents();
        setNavigationEvents();

        //Установка события клика по навигации
        function setNavigationEvents() {
            var classname = document.getElementsByClassName("navigation");
            for (var i = 0; i < classname.length; i++) {
                classname[i].addEventListener('click', clickNavigation, false);
            }
        }

        //Установка события клика по пагинации
        function setPaginationEvents() {
            var classname = document.getElementsByClassName("page-paginator");
            for (var i = 0; i < classname.length; i++) {
                classname[i].addEventListener('click', clickPagination, false);
            }
        }

        //Очистка таблицы и пагинации
        function clear() {
            var elems = document.getElementsByClassName('data');
            while(elems[0]) {
                elems[0].parentNode.removeChild(elems[0]);
            }

            elems = document.getElementById('paginator');
            elems.innerHTML = "";
        }

        //Функция посылает аякс-запрос на получение сотрудников
        function getData(path) {
            clear();
            var xhr = new XMLHttpRequest();
            xhr.open('GET', path, false);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            xhr.send();
            if (xhr.status != 200) {
                alert('error');
            } else {
                window.history.pushState(null, null, path);
                var json = JSON.parse(xhr.response);
                var doc = document.getElementById("header");
                doc.insertAdjacentHTML("afterEnd", json['data']);

                doc = document.getElementById("paginator");
                doc.insertAdjacentHTML("beforeend", json['pagination']);

                //Первый аякс-запрос возвращает json, при повторной загрузки страницы мы снова получим json-объект.
                //Для решения данной проблемы посылаем второй запрос, который возвращает всю страницу.
                xhr = new XMLHttpRequest();
                xhr.open('GET', path, false);
                xhr.send();

                setPaginationEvents();
            }
        }

        //Данная функция вызывается при клике по пагинации
        function clickPagination() {
            var page = this.getAttribute('data-page');
            var path = window.location.pathname;

            if(path.length == 1) {
                path = '/db/employees/' + page;
                path += '?quantity=' + getQuantity();
            } else {
                var url_arr = path.split('/');
                var last = url_arr[url_arr.length - 1];
                if(last.match(/[0-9]/)) {
                    url_arr[url_arr.length - 1] = page;
                    path = url_arr.join("/");
                } else {
                    if(path.slice(-1) == "/") {
                        path += page;
                    } else {
                        path += "/" + page;
                    }
                }
                path += '?quantity=' + getQuantity();
            }

            getData(path);
        }

        //Данная функция вызывается при клике по навигации
        function clickNavigation() {
            var path = this.getAttribute('data-href');
            path += "?quantity=" + getQuantity();
            getData(path);
        }

        //Изменение количества вывода сотрудников на странице
        function changeQuantity() {
            var path = window.location.pathname;
            if(path.length == 1) {
                path = '/db/employees?quantity=' + getQuantity();
            } else if(path.length > 1){
                if(path.indexOf('?') > 1) {
                    path = path.split('?');
                    path = path[0] + '?quantity=' + getQuantity();
                } else {
                    var url_arr = path.split('/');
                    var last = url_arr[url_arr.length - 1];
                    if(last.match(/[0-9]/)){
                        url_arr[url_arr.length - 1] = 1;
                        path = url_arr.join("/");
                    }
                    path += '?quantity=' + getQuantity();
                }
            }
            getData(path);
        }

        //Функция возвращает допустимое количество сотрудников на страницу
        function getQuantity() {
            var e = document.getElementById("quantity");
            return e.options[e.selectedIndex].value;
        }
    </script>
    <style>
        /* Pagination style */
        .custom-paginator {
            text-align: center;
        }
        .pagination {margin:0;padding:0;}
        .pagination li {
            display: inline;
            padding: 6px 10px 6px 10px;
            border: 1px solid #ddd;
            margin-right: -1px;
            font: 15px/20px Arial, Helvetica, sans-serif;
            background: #FFFFFF;
            box-shadow: inset 1px 1px 5px #F4F4F4;
        }
        .pagination li a {
            text-decoration:none;
            color: rgb(89, 141, 235);
        }
        .pagination li.first {
            border-radius: 5px 0px 0px 5px;
        }
        .pagination li.last {
            border-radius: 0px 5px 5px 0px;
        }
        .pagination li:hover {
            background: #CFF;
        }
        .pagination li.active {
            background: #F0F0F0;
            color: #333;
        }
        .page-paginator {
            cursor:pointer;
        }
        span.navigation {
            cursor: pointer;
        }
    </style>
</body>
</html>