<?php
class EmployeeMonthlyRate implements EmployeeInterface {
    public static function getSalary($payment) {
        if(is_numeric($payment)) {
            return $payment;
        }
        return 0;
    }
}