<?php
include 'EmployeeInterface.php';
include 'EmployeeHourlyRate.php';
include 'EmployeeMonthlyRate.php';

class Employees {
    const DATA_DB       = 'db';
    const DATA_XML      = 'xml';
    const PAYMENT_HOUR  = 1;
    const PAYMENT_MONTH = 2;

    private $connect;
    private $table;
    private $paginator;
    private $quantity;
    private $page;

    public function __construct() {

        $this->paginator = '';
        $this->table     = '';

        if(isset($_GET['data_type']) && ($_GET['data_type'] == self::DATA_DB || $_GET['data_type'] == self::DATA_XML)) {

            $this->connect = mysqli_connect("localhost", "root", "", "employees");
            if (mysqli_connect_errno()) {
                include 'error/404.php';
                die;
            }

            if(isset($_GET['quantity']) && is_numeric($_GET['quantity'])) {
                $this->quantity = $_GET['quantity'];
            } else $this->quantity = 20;

            if(isset($_GET['page']) && $_GET['page']) {
                $this->page = ($_GET['page'] - 1) * $this->quantity;
            } else $this->page = 0;

            $this->generateEmployeesTable();
        } else {            
            include 'error/404.php';
            die;
        }

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) {
            if(empty($this->getRows())) {
                return false;
            }
            $result = [
                'data'       => $this->getRows(),
                'pagination' => $this->getPagination()
            ];
            echo json_encode($result);
            die;
        }
    }

    //Метод возвращает разметку таблицы с данными о сотрудниках
    public function getRows() {
        return $this->table;
    }

    //Метод возвращает разметку пагинации
    public function getPagination() {
        return $this->paginator;
    }

    //Метод создает таблицу сотрудников
    private function generateEmployeesTable() {
        if(isset($_GET['department'])) {
            $department_id = $this->getDepartmentIdByName($_GET['department']);
            if(!$department_id) {
                include 'error/404.php';
                die;
            }
        }

        if(isset($_GET['position']) && isset($department_id)) {
            $position_id = $this->getPositionIdByName($department_id, $_GET['position']);
            if(!$position_id) {
                include 'error/404.php';
                die;
            }
        }

        if($_GET['data_type'] == self::DATA_DB) {

            $sql_count     = "SELECT COUNT(empl.employee_id) as 'count' ";
            $sql_employees = "SELECT SQL_CALC_FOUND_ROWS empl.full_name, empl.birthday, dp.name as 'department', ps.name as 'position', 
                              empl.payment, empl.payment_type_id, tp.name as 'payment_type' ";

            $sql = "FROM employees empl
                    LEFT JOIN departments dp ON (dp.department_id = empl.department_id)
                    LEFT JOIN positions ps ON (ps.position_id = empl.position_id)
                    LEFT JOIN type_payments tp ON (tp.payment_id = empl.payment_type_id)
                    WHERE empl.payment > 0";

            $param_count = 0;
            if(isset($department_id)) {
                $sql .= " AND dp.department_id = ?";
                $param_count++;
            }

            if(isset($position_id)) {
                $sql .= " AND ps.position_id = ?";
                $param_count++;
            }

            $sql_query = $sql_count . $sql;

            $stmt = $this->connect->prepare($sql_query);
            if(!$stmt) {
                return false;
            }

            if($param_count == 1) {
                $stmt->bind_param('i',$department_id);
            } elseif($param_count == 2) {
                $stmt->bind_param('ii',$department_id,$position_id);
            }

            $stmt->execute();
            $result_count = $stmt->get_result();
            $result_count = $result_count->fetch_assoc();

            if(!empty($result_count)) {

                $employees_count = $result_count['count'];
                if($employees_count > 0) {

                    $sql .= " LIMIT " . $this->page . ", " . $this->quantity;
                    $sql_query = $sql_employees . $sql;
                    $stmt = $this->connect->prepare($sql_query);

                    if($param_count == 1) {
                        $stmt->bind_param('i', $department_id);
                    } elseif($param_count == 2) {
                        $stmt->bind_param('ii', $department_id, $position_id);
                    }

                    $rows = '';
                    $stmt->execute();
                    $one_employee = $stmt->get_result();

                    while($one_record = $one_employee->fetch_assoc()) {
                        if($one_record['payment_type_id'] == self::PAYMENT_HOUR) {
                            $salary = EmployeeHourlyRate::getSalary($one_record['payment']);
                        } elseif($one_record['payment_type_id'] == self::PAYMENT_MONTH) {
                            $salary = EmployeeMonthlyRate::getSalary($one_record['payment']);
                        } else {
                            $salary = 0;
                        }
                        $one_record['salary'] = $salary;

                        $rows .= $this->generateOneRow($one_record);
                    }

                    $this->table = $rows;
                    $this->paginator = $this->createPagination((isset($_GET['page'])) ? $_GET['page'] : 1, ceil($employees_count / $this->quantity));
                }
            }

            $stmt->free_result();
            $stmt->close();
        } else {
            $xmlString = file_get_contents('xml_data/employees.xml');
            $xml       = new SimpleXMLElement($xmlString);

            $json      = json_encode($xml);
            $employees = json_decode($json, true);

            if(isset($employees['employee']) && !empty($employees['employee'])) {

                $rows      = '';
                $employees_array = array_slice($employees['employee'], $this->page, $this->quantity);

                foreach ($employees_array as $employee) {

                    if ($employee['payment_type_id'] == self::PAYMENT_HOUR) {
                        $salary = EmployeeHourlyRate::getSalary($employee['payment']);
                    } elseif ($employee['payment_type_id'] == self::PAYMENT_MONTH) {
                        $salary = EmployeeMonthlyRate::getSalary($employee['payment']);
                    } else {
                        $salary = 0;
                    }
                    $employee['salary'] = $salary;

                    $rows .= $this->generateOneRow($employee);
                }
                $this->table = $rows;

                $this->paginator = $this->createPagination((isset($_GET['page'])) ? $_GET['page'] : 1, ceil(count($employees['employee']) / $this->quantity));
            }
        }
    }

    //Метод проверяет на существование отдела и возвращает его id в случае нахождения
    private function getDepartmentIdByName($department_name) {
        if($department_name) {
            $stmt = $this->connect->prepare("SELECT department_id FROM departments WHERE url_name = ? LIMIT 1");
            if($stmt) {
                $stmt->bind_param('s', $department_name);
                $stmt->execute();

                $result = $stmt->get_result();
                $result = $result->fetch_assoc();
                if (!empty($result)) {
                    return $result['department_id'];
                }
            }
        }
        return false;
    }

    //Метод проверяет на существование должности в отделе и возвращает его id в случае нахождения
    private function getPositionIdByName($department_id, $position) {
        if($department_id && $position) {
            $stmt = $this->connect->prepare("SELECT position_id FROM positions WHERE department_id = ? AND url_name = ? LIMIT 1");
            if($stmt) {
                $stmt->bind_param('is',$department_id,$position);
                $stmt->execute();

                $result = $stmt->get_result();
                $result = $result->fetch_assoc();
                if(!empty($result)) {
                    return $result['position_id'];
                }
            }
        }
        return false;
    }

    //Создание пагинации
    private function createPagination($current_page, $total_pages)
    {
        $pagination = '';
        if($current_page > $total_pages) {
            return null;
        }

        $difference = $total_pages - $current_page;
        $low_range  = $current_page - 5;
        $high_range = $current_page + 6;

        $pagination .= '<ul class="pagination">';
        if ($total_pages <= 10) {

            for ($i = 1; $i <= $total_pages; $i++) {
                if($current_page == $i) {
                    $pagination .= '<li class="active">' . $i . '</li>';
                } else {
                    $pagination .= '<li><span class="page-paginator" data-page="' . $i . '" title="Page ' . $i . '">' . $i . '</span></li>';
                }
            }
        } else if ($total_pages > 10 && $difference < 5) {

            for ($i = ($total_pages - 10); $i <= $total_pages; $i++) {
                if($current_page == $i) {
                    $pagination .= '<li class="active">' . $i . '</li>';
                } else {
                    $pagination .= '<li><span class="page-paginator" data-page="' . $i . '" title="Page ' . $i . '">' . $i . '</span></li>';
                }
            }
        } else if ($total_pages > 10) {
            if ($current_page < 6) {
                for ($i = 1; $i < 12; $i++) {
                    if($current_page == $i) {
                        $pagination .= '<li class="active">' . $i . '</li>';
                    } else {
                        $pagination .= '<li><span class="page-paginator" data-page="' . $i . '" title="Page ' . $i . '">' . $i . '</span></li>';
                    }
                }
            } else {
                for ($i = $low_range; $i < $high_range; $i++) {
                    if($current_page == $i) {
                        $pagination .= '<li class="active">' . $i . '</li>';
                    } else {
                        $pagination .= '<li><span class="page-paginator" data-page="' . $i . '" title="Page ' . $i . '">' . $i . '</span></li>';
                    }
                }
            }
        }
        $pagination .= '</ul>';

        return $pagination;
    }

    //Данный метод используется в создании разметки таблицы и возвращает одну строку таблицы
    private function generateOneRow($data) {
        $row = '';
        if(!empty($data)) {
            $row .= "<tr class='data'>";
            $row .= "<td>" . $data['full_name'] . "</td>";
            $row .= "<td>" . $data['birthday'] . "</td>";
            $row .= "<td>" . $data['department'] . "</td>";
            $row .= "<td>" . $data['position'] . "</td>";
            $row .= "<td>" . $data['payment_type'] . "</td>";
            $row .= "<td>" . $data['salary'] . "</td>";
            $row .= "</tr>";
        }
        return $row;
    }

    //Метод возвращает допустимое количество сотрудников на страницу
    public function getQuantity() {
        return $this->quantity;
    }
}