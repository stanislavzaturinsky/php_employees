<?php
class EmployeeHourlyRate implements EmployeeInterface {
    public static function getSalary($payment) {
        //для примера возьмем месяц в котором 30 дней, отработанных 22 рабочих дня
        if(is_numeric($payment)) {
            return $payment * 8 * 22;
        }
        return 0;
    }
}